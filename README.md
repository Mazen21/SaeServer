/**************** SAE server automation test *****************************/

============================
I/Introduction
============================

This project is used to simulate sae server by simplifying frame creation according 
to the AMVSTYPE specified by the user. Following are the AMVSTYPE handled:

============================================
AMVSTYPE		Client			Protocol
============================================
	2			UDP				UDP
	3			NICE			TCP
	4			TISSEO			TCP
	5			RATP			SERIAL
	6			DIVIA			UDP
 	7			FCO				#
============================================
Before test execution the following configuration must be defined at the 
start of the main python file "saeserver_generic.py"

following an example of the configuration section defined for TISSEO.

#~ Configuration 
TGU_IP = '192.168.0.8'  				# TGU address
TCP_IP = '192.168.0.6'  				# Your machine address
TCP_PORT = 6004         				# TCP port used for AMVSTYPE 4 and 3
UDP_PORT = 33334						# UDP port used for AMVSTYPE 2 and 6
BUFFER_SIZE = 1024						#not relevant
AMVSTYPE = '4'							#Default AMVSTYPE
FRAMETYPE = '10'						#Default Frametype
offset = 3								#number of bytes before data template in tra frame
tgu_logfile ="/logs/sae_service.log"	#file used to log sae services under TGU file system
home_logfile = "/home/mazen/sae.log"	#file used to log sae services under your machine
#~ End Configuration

============================
II/How to use the script
============================

1/ Change TCP_IP variable with the address of the SAE server

2/ Change TCP_PORT variable with port used for tcp communication

3/ Add unit test in ut_tis.py

4/ run gen_logger.py to trace log file

4/ run ut_tis.py for tisseo unit tests

============================
III/ How to format a test
============================

writing tests should be in ut_tis.py (for tisseo).

Test must follow the following format :

##1/ Creating an instance of the frame with frame type mentionning
exmpl : f = TisFrame(11)

##2/ By default a instance of a frame has default values that can be checked in the 
relative dataFrameTemplate along with other values such as range and offset.
DataFrameTemplate is defined as an attribute in the frame class.

exmpl for Time frame type for TISSEO:
print ("|offset|SIZE|Actual Value |Default Value   |min Value |Maximu Value|")
	m_HeureDataFrameTemplate = [
					("ANNEE",[7,1,118,118,0,255]),
					("MOIS",[8,1,1,1,1,12]),
					("JOUR",[9,1,1,1,1,31]),
					("HEURE",[10,1,1,1,0,23]),
					("MINUTE",[11,1,1,1,0,59]),
					("SECONDES",[12,1,1,1,0,59]),

##3/ By default the actual Value is equal to the default value. Should you want to change it
use the method setFiel by specifiying the field name and the new Value.
exmpl :
f.set_field("NUM_COND",3)

##4/ Before you send the data to TGU, It is possible to check the frame by using the method print
tis_print for Tisseo
Exmpl : 
f.tis_print()

##5/To send the frame use metho send
f.send('192.168.0.6',TCP_PORT) specifiying the address of your machine and the relative port

##6/Finally to check wether the tra frame is correct use method checkExpectedTRA by specifying the expected TRA message as a string
checkExpectedTRA("17 0B 03 00 00 00 00 00 00 03 00 00 00 00 00 FF 00 00 00 00 00 00 00",os.path.basename(__file__),sys._getframe().f_code.co_name,"step 1")

==============================================
IV / Update the frame template
==============================================

In order to update a default value or the range of a field the fram template, simply modified directly in the
attribute variable in the relative class.
Exmpl : 
If you want to update the range of the NUM_CRSE field in the course frame from (0 - 99) to (10 -255):
("NUM_CRSE",[52,1,0,0,0,99])  ==>  ("NUM_CRSE",[52,1,0,0,10,255])



