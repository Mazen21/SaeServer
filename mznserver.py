#!/usr/bin/env python
 
import socket
import sys
 
TCP_IP = '192.168.0.6'
TCP_PORT = 6004
BUFFER_SIZE = 1024  # Normally 1024, but we want fast response

data1 = '\xFF\xFF\xFF\x02\x00\x0f\x0A\x01\x1a\x03\x04\x05\x06\xFF\x03'
data='\xff\xff\xff\x02\x00\x1F\x0B\x75\x06\x05\x08\x08\x08\x00\x00\x00\x04\x00\x00\x01\x04\xff\xff\xff\xff\xff\xff\xff\xff\xff\x03'
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((TCP_IP, TCP_PORT))
s.listen(1)

conn, addr = s.accept()
conn.send(data1)
	
conn.close()
