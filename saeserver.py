#!/usr/bin/env python
 
import socket
import sys
import binascii
from saeserver_generic import *
from ineo_sae import *
from nce_sae import *
from tis_sae import *
from ratp_sae import *
from udp_sae import *

def INEO_SAE():
	print ("INEO_SAE")	
def NIS_SAE():
	print ("NIS_SAE")
def RATP_SAE():
	print ("RATP_SAE")
def DIVIA_SAE():
	print ("DIVIA_SAE")
	binary_string = binascii.unhexlify("1A")
	print (binary_string)
	

if (len(sys.argv) < 2):
	noArgsMessage()
else:
	AMVSTYPE = sys.argv[1]
	if(AMVSTYPE == '2'):
		INEO_SAE()
	elif(AMVSTYPE == '3'):
		NIS_SAE()
	elif(AMVSTYPE == '4'):
		TIS_SAE()
	elif(AMVSTYPE == '5'):
		RATP_SAE()
	elif(AMVSTYPE == '6'):
		DIVIA_SAE()
	else:
		print ("AMVSTYPE is not recognized")
