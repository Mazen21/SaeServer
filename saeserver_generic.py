#!/usr/bin/env python
import socket
import sys
import binascii
import collections
import time

#~ Configuration 
UDP_IP = "192.168.0.8"
TGU_IP = '192.168.0.8'  # TGU address
TCP_IP = '192.168.0.6'  # Your machine address
TCP_PORT = 6004         # TCP port used for AMVSTYPE 4 and 3
UDP_PORT = 33334		# UDP port used for AMVSTYPE 2 and 6
BUFFER_SIZE = 1024		# Not relevant
AMVSTYPE = '4'			#Default AMVSTYPE
FRAMETYPE = '10'		#Default Frametype
offset = 3				#number of bytes before data template in tra frame
tgu_logfile ="/logs/sae_service.log"	#file used to log sae services under TGU file system
home_logfile = "/home/mazen/sae.log"	#file used to log sae services under your machine
#~ End Configuration

def TextArrayToBytes(testArray,length):
	value = 0
	power = length -1 
	print power
	#~ for x in testArray:
		#~ 
	print "end"

def to_bytes(n, length, endianess='big'):
    h = '%x' % n
    s = ('0'*(len(h) % 2) + h).zfill(length*2).decode('hex')
    return s if endianess == 'big' else s[::-1]

def convertToDataByte(tmplate,HexString):
	dataByte= [0] * len(tmplate)
	data_size = len(tmplate)
	for x in range(0,data_size):
		field = int(HexString[x],16)
		dataByte[x] = to_bytes(field,tmplate[x],'big')
	return dataByte

def convertToDataByte2(data_dict):
	dataByte=  [0] * len(data_dict)
	data_size = len(data_dict)
	for x, itm in enumerate(data_dict):
		dataByte[x] = to_bytes(itm[1][3],itm[1][1],'big')
	return dataByte

def noArgsMessage():
	print ("./saeserver.py [AMVS TYPE] [FRAME TYPE] [DATA]")

def sendTCPData(data,tisIP,tisPORT):
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		s.bind((tisIP, tisPORT))
		s.listen(1)
		conn, addr = s.accept()
		conn.send(''.join(data))
		conn.close()
		
def sendUDPData(data,UDP_IP,UDP_PORT):
	#~ MESSAGE = '\x28\x01\x00\x00\x00\x00\x00\x00\x00\x00\x7A\x00\x00\x00\x00\x01\x16\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x30\x30\x31\x32\x33\x34' 
	#~ print "Data to send {0}".format(data)
	sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
	sock.sendto(''.join(data), (UDP_IP, UDP_PORT))



def checkExpectedTRA(tra,file_name,func_name,step):
	print "expected TRA : {0}".format(tra)
	time.sleep(3)
	for line in reversed(open(home_logfile).readlines()):
		if "TRA to send :{0}".format(tra) in line.rstrip():
			print "{0}	:{1}	:{2} OK ".format(file_name,func_name,step)
			return 1
	print "{0}	:{1}	:{2} failed ".format(file_name,func_name,step)
	return 0
