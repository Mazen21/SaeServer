#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <pthread.h>
#include <termios.h>
#include <errno.h>
 #include <string.h>
//#define RATP_LOC
#define RATP
//#define TRA
int serial_set_interface_attribs (int fd, int speed, int parity, int should_block)
{
   struct termios tty;
   memset (&tty, 0, sizeof tty);
   if (tcgetattr (fd, &tty) != 0)
   {
            return -1;
   }

   cfsetospeed (&tty, speed);
   cfsetispeed (&tty, speed);

   tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
   // disable IGNBRK for mismatched speed tests; otherwise receive break
   // as \000 chars
   tty.c_iflag &= ~IGNBRK;         // disable break processing
   tty.c_iflag &= ~ICRNL;          //Do NOT convert 0xD to 0xA !!!
   tty.c_lflag = 0;                // no signaling chars, no echo,
                                    // no canonical processing
   tty.c_oflag = 0;                // no remapping, no delays
   tty.c_cc[VMIN]  = 0;            // read doesn't block
   tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

   tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

   tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
                                    // enable reading
   tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
   tty.c_cflag |= parity;
   tty.c_cflag &= ~CSTOPB;
   tty.c_cflag &= ~CRTSCTS;
   
   tty.c_cc[VMIN]  = should_block ? 1 : 0;
   tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

   if (tcsetattr (fd, TCSANOW, &tty) != 0)
   {
            // // log_err ("error %d from tcsetattr\n", errno);
            return -1;
   }
   return 0;
}


int serial_openPort(char * pcPortName, int iBaudrate)
{
   int fd = open (pcPortName, O_RDWR | O_NOCTTY | O_SYNC);
   if (fd < 0)
   {
      // log_err ("error %d opening %s: %s\n", errno, pcPortName, strerror (errno));
      return -1;
   }

   serial_set_interface_attribs (fd, iBaudrate, 0, 0);  // set speed to 115,200 bps, 8n1 (no parity)

   return fd ;
}

// #define RS232_3WIRES          "/dev/ttymxc1"
#define RS232_3WIRES          "/dev/ttyUSB0"
int openPort(const char *dev_tty,int baud)
{
	int fd;
	int n = 0;
	struct termios tty;

    // open the tty
    fd = open(dev_tty,O_RDWR|O_NDELAY|O_NOCTTY);  //open port for Read and Write
    
	if (fd >= 0) {
		/* Cancel the O_NDELAY flag. */
		n = fcntl(fd, F_GETFL, 0);
		fcntl(fd, F_SETFL, n & ~O_NDELAY);
	}
		if (fd < 0) {
		perror("open");
		return fd;
	}

	tcgetattr(fd, &tty);
	//set the baud rate
	cfsetospeed(&tty, (speed_t)baud);
	cfsetispeed(&tty, (speed_t)baud);
	
	tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;
	
	/* Set into raw, no echo mode */
	//tty.c_iflag =  IGNBRK;
	tty.c_lflag = 0;
	tty.c_oflag = 0;
	//tty.c_cflag |= CLOCAL | CREAD;
	tty.c_cc[VMIN] = 0;
	tty.c_cc[VTIME] = 0;
	
	tty.c_iflag &= ~(IXON|IXOFF|IXANY);
	
	//tty.c_cflag &= ~(PARENB | PARODD);
	#ifdef RATP
	        tty.c_cflag &= ~PARENB;
	        	tty.c_cflag &= ~CSTOPB;
	        	tty.c_iflag = IGNPAR;
	        #endif
        
	

	tty.c_cflag &= ~CRTSCTS;
	#ifdef TRA
			tty.c_cflag |= PARENB;
tty.c_cflag &= ~PARODD;	
tty.c_iflag |= INPCK;
			tty.c_iflag |= IGNPAR;
			#endif
	tcsetattr(fd, TCSANOW, &tty);
	return fd;
}
//============================================================================//
// ROLE : write data over the device
//----------------------------------------------------------------------------//
// INPUT : 	 char *device device descriptor
//		char buffer : data
//		int size : number of bytes that you want to write
// OUTPUT : 	bytes written
//
// RESOURCES :
//----------------------------------------------------------------------------//
// READ ONLY VARIABLES:
//
// MODIFIED VARIABLES:
//
// KERNEL SERVICES:
//
// SUB-FUNCTIONS:
//
//----------------------------------------------------------------------------//
// HIST : Version |   Date   | Author | Description
//        --------------------------------------------------------------------
//                |          |        |
//----------------------------------------------------------------------------//
// ALGO :
//============================================================================//	
int serial_write(int device,char *buffer, int size)
{
	int bytes_write ;
	bytes_write = write(device,buffer,size);
	printf("buffer write is %s\n",buffer);  
	return bytes_write;
}
//============================================================================//
// NAME : rs232_read
//============================================================================//
// ROLE : read data from the device
//----------------------------------------------------------------------------//
// INPUT : 	 char *device device descriptor
//		char buffer : data
//		int nb_bytes : number of bytes that you want to recieve 
// OUTPUT : 	bytes read
//
// RESOURCES :
//----------------------------------------------------------------------------//
// READ ONLY VARIABLES:
//
// MODIFIED VARIABLES:
//
// KERNEL SERVICES:
//
// SUB-FUNCTIONS:
//
//----------------------------------------------------------------------------//
// HIST : Version |   Date   | Author | Description
//        --------------------------------------------------------------------
//                |          |        |
//----------------------------------------------------------------------------//
// ALGO :
//============================================================================//	
int serial_read (int device,char *buffer,int nb_bytes)
{
	int bytes_read =0 ;
	do
	{
		bytes_read += read(device,&buffer[bytes_read],nb_bytes-bytes_read);
		printf("%d\n",bytes_read);
	}while(bytes_read<nb_bytes); 
	buffer[bytes_read+1]=0;
	printf("buffer read is %s\n",buffer);  
	return bytes_read;
}
void main(void)
{
		//char buffer[16] ="Hello World!!!!";
	char read_buffer[512]={0};
   int l_iRs3wire_fd=-1 ;
   int bytes_written =0, bytes_read=0;
   char pcSendBuffer[128]={0};
   char pcReceiveBuffer=0;
   
   l_iRs3wire_fd = -1 ;
 #ifdef RATP
	int baud = B4800;
	#endif
#ifdef TRA
		int baud = B9600;
		#endif
   l_iRs3wire_fd = openPort("/dev/ttyUSB0", baud);
   if (l_iRs3wire_fd == -1)
   {
      printf("Error: Failed to Open %s \n", RS232_3WIRES);
   }
   else
   {
      
      memset(pcSendBuffer, 0x00, 128);
    //  memset(pcReceiveBuffer, 0x00, 128);
      
      int i =0;
#ifdef TRA   // [15] [0B] [10] [03] [00] [10] [10] [18] [D2] [00] [01] [00] [6F]  [00] [0C] [00] [F9] [00] [01] [0C] [00]
//[15] [0B] [01] [00] [10] [10] [18] [D2] [00] [01] [00] [00] [00] [00] [00] [00] [00] [00] [00] [00] [00]
//[15] [0B] [02] [00] [10] [18] [D2] [00] [01] [00] [6F] [00] [0C] [00] [F9] [00] [01] [0C] [00] [02] [B6]
//17 0B 03 00 64 10 18 00 D2 10 01 00 6F 00 FF FF 00 F9 17 00 0C 00 00
   pcSendBuffer[0] = 0x15; //'STX'
      pcSendBuffer[1] = 0x0B;
       //open TRA session
      pcSendBuffer[2] = 0x10; //pcSendBuffer[i++] = 'P';
    
      pcSendBuffer[3] = 0x03; //pcSendBuffer[i++] = 'K';*/
      /**close TRA session**/
    /*  pcSendBuffer[2] = 0x00; //pcSendBuffer[i++] = 'P';
     
      pcSendBuffer[3] = 0x00; //pcSendBuffer[i++] = 'K';*/
      pcSendBuffer[4] = 0x00;
      /*PARK*/
       pcSendBuffer[5] = 0x10 ;
       pcSendBuffer[6] = 0x10 ;
       pcSendBuffer[7] =  0x18 ;
       /*driver*/
      
       pcSendBuffer[8] = 0xD2 ;

       pcSendBuffer[9] = 0x10 ;
              /*service*/
       pcSendBuffer[10] = 0x01;
        pcSendBuffer[11] = 0x00;
       
       /*course*/
       pcSendBuffer[12] = 0x6F ;
       pcSendBuffer[13] = 0x00 ;
       /*network*/
       pcSendBuffer[14] = 0x0C ;
       pcSendBuffer[15] = 0x00 ;
       /*Line*/
       pcSendBuffer[16] = 0xF9 ;
       pcSendBuffer[17] = 0x10;
       /*subline*/
       pcSendBuffer[18] = 0x17 ;
       pcSendBuffer[19] = 0x00 ;
       /*sens*/
       pcSendBuffer[20] = 0x0C ;
       /*Stop bus ID*/
       pcSendBuffer[21] = 0x00 ;
       pcSendBuffer[22] = 0x00 ;
       pcSendBuffer[23] = 0x02 ;
       //pcSendBuffer[21] = 0xB6;
#endif
#ifdef RATP
       pcSendBuffer[0] = 0x02; //'STX'
       pcSendBuffer[1] = 0xFB;

       pcSendBuffer[2] = 0x00; //pcSendBuffer[i++] = 'P';
       pcSendBuffer[3] = 34; //pcSendBuffer[i++] = 'K';
       pcSendBuffer[4] = 44;     
      for(i=5;i<49;i++)
       pcSendBuffer[i] = 0x34 ;
       
       pcSendBuffer[18] = 0x30 ;
       pcSendBuffer[19] = 0x30 ;
       pcSendBuffer[20] = 0x30  ;
       for(i=1;i<49;i++)
		pcSendBuffer[49] ^= pcSendBuffer[i];
       //pcSendBuffer[21] = 0xFC ;
       pcSendBuffer[50] = 0x03;
#endif
#ifdef RATP_LOC
       pcSendBuffer[0] = 0x02; //'STX'
       pcSendBuffer[1] = 0xFB;

       pcSendBuffer[2] = 0x00; //pcSendBuffer[i++] = 'P';
       pcSendBuffer[3] = 34; //pcSendBuffer[i++] = 'K';
       pcSendBuffer[4] = 44;     
      for(i=5;i<49;i++)
       pcSendBuffer[i] = 0x34 ;
       for(i=1;i<49;i++)
		pcSendBuffer[49] ^= pcSendBuffer[i];
       //pcSendBuffer[21] = 0xFC ;
       pcSendBuffer[50] = 0x03;
#endif

//while(1)
//{
      bytes_written = serial_write(l_iRs3wire_fd, pcSendBuffer, 52);

      if (bytes_written > 0)
      {
         printf("%d bytes sent successfully\n", bytes_written);
         
			/*if (l_iRs3wire_fd == -1)
			   {
				  printf("Error: Failed to Open %s \n", RS232_3WIRES);
			   }
			   else
			   {
				 bytes_read = serial_read(l_iRs3wire_fd,read_buffer,34);
					if (bytes_read > 0)
					 {
						printf("ReceivedData: %s\n", read_buffer);
					 }
					 else
					 {
						 printf("Error: Failed to read from serial port bytes_read=%d, errno=%d  ERROr=%s\n",bytes_read, errno, strerror(errno));
					 }
					 close(l_iRs3wire_fd);
				 }*/
        /* sleep(2);
			 pcReceiveBuffer=0;
			 bytes_read = read(l_iRs3wire_fd, &pcReceiveBuffer, 1);
			 printf("%d\n",bytes_read);
			 if (bytes_read > 0)
			 {
				printf("ReceivedData: %c\n", pcReceiveBuffer);
			 }
			 else
			 {
				 printf("Error: Failed to read from serial port bytes_read=%d, errno=%d  ERROr=%s\n",bytes_read, errno, strerror(errno));
			 }
			 bytes_read++;*/
		// }
            
      }
      else
      {
         printf("Error: Failed to send data to serial port\n");
      }
      sleep(1);
//  }
  close(l_iRs3wire_fd);
   }
   close(l_iRs3wire_fd);  
   
}

//END OF FILE
