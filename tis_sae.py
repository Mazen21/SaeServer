#!/usr/bin/env python
import socket
import sys
import binascii
from saeserver_generic import *

def noArgsMessage_tis_FrameType():
	print ("Heure\t\t\t\t10")
	print ("Localisation\t\t\t11")
	print ("Course\t\t\t\t12")
	print ("Arret\t\t\t\t13")
	print ("Information_voyageurs\t\t14")
	print ("Etat_Detresse\t\t\t15")
	print ("Localisation_GPS\t\t16")
	print ("Etat_capteur_localisation\t17")
	print ("Infomation_deviation\t\t18")

def noArgsMessage_tis_HeureData():
	print ("|offset|Taille|ID |Champ   |intervalle|Default|")
	print ("|7     |1     |0  |ANNE    |0<->255   |100    |")
	print ("|8     |1     |1  |MOIS    |1<->12    |1      |")
	print ("|9     |1     |2  |JOUR    |1<->31    |1      |")
	print ("|10    |1     |3  |HEURE   |0<->23    |1      |")
	print ("|11    |1     |4  |MINUTE  |0<->59    |1      |")
	print ("|12    |1     |5  |SECONDES|0<->59    |1      |")

def noArgsMessage_tis_LocalisationData():
	print ("|offset|Taille|ID |Champ      |intervalle|Default|")
	print ("|7     |1     |0  |ANNE       |0<->255   |100    |")
	print ("|8     |1     |1  |MOIS       |1<->12    |1      |")
	print ("|9     |1     |2  |JOUR       |1<->31    |1      |")
	print ("|10    |1     |3  |HEURE      |0<->23    |1      |")
	print ("|11    |1     |4  |MINUTE     |0<->59    |1      |")
	print ("|12    |1     |5  |SECONDES   |0<->59    |1      |")
	print ("|13    |4     |6  |NUM_COND   |0<->99999 |0      |")
	print ("|17    |2     |8  |NUM_PARC   |0<->65535 |0      |")
	print ("|19    |1     |8  |ETAT_EXPL  |0<->3     |0      |")
	print ("|20    |1     |9  |ETAT_LOC   |0<->10    |1      |")
	print ("|21    |1     |10 |TPS_ATT_TER|0<->99    |1      |")
	print ("|22    |1     |11 |NUM_CRSE   |0<->99    |1      |")
	print ("|23    |3     |12 |NUM_ARRET  |0<->99999 |1      |")
	print ("|26    |1     |13 |ZONE       |0<->255   |1      |")
	print ("|27    |1     |14 |DEPOT      |0<->1     |1      |")
	print ("|28    |1     |15 |TYPE_VEHIC |1 ou 4    |1      |")

def noArgsMessage_tis_CourseData():
	print ("|offset|Taille|ID |Champ      |intervalle|Default|")
	print ("|7     |1     |0  |ANNE       |0<->255   |100    |")
	print ("|8     |1     |1  |MOIS       |1<->12    |1      |")
	print ("|9     |1     |2  |JOUR       |1<->31    |1      |")
	print ("|10    |1     |3  |HEURE      |0<->23    |1      |")
	print ("|11    |1     |4  |MINUTE     |0<->59    |1      |")
	print ("|12    |1     |5  |SECONDES   |0<->59    |1      |")	
	print ("|13    |4     |6  |NUM_LIGNE  |TEXT      |MNLG   |")	
	print ("|17    |2     |7  |NUM_CHAINE |0<->9999  |1      |")	
	print ("|19    |16    |8  |DESTINATION|TEXT      |TOULOUS|")	
	print ("|35    |16    |9  |VIA        |TEXT      |VIA    |")	
	print ("|51    |1     |10 |LOG_ACTIVE |0<->2     |0      |")	
	print ("|52    |1     |11 |NUM_CRSE   |0<->99    |0      |")	
	print ("|53    |1     |12 |SENS       |0<->2     |0      |")

def noArgsMessage_tis_ArretData():
	print ("|offset|Taille|ID |Champ      |intervalle|Default|")
	print ("|7     |1     |0  |ANNE       |0<->255   |100    |")
	print ("|8     |1     |1  |MOIS       |1<->12    |1      |")
	print ("|9     |1     |2  |JOUR       |1<->31    |1      |")
	print ("|10    |1     |3  |HEURE      |0<->23    |1      |")
	print ("|11    |1     |4  |MINUTE     |0<->59    |1      |")
	print ("|12    |1     |5  |SECONDES   |0<->59    |1      |")	
	print ("|13    |1     |6  |POSITION   |0<->3     |0      |")	
	print ("|14    |1     |7  |NBR_ARRET  |0<->5     |0      |")	
	print ("|15    |3     |8  |NUM_ARRET1 |0<->99999 |1      |")	
	print ("|18    |3     |9  |NUM_ARRET2 |0<->99999 |2      |")	
	print ("|21    |3     |10 |NUM_ARRET3 |0<->99999 |3      |")	
	print ("|24    |3     |11 |NUM_ARRET4 |0<->99999 |4      |")	
	print ("|27    |3     |12 |NUM_ARRET5 |0<->99999 |5      |")

def TIS_FormatData(tis_type,length,tis_raw_data):
		tisData = [0]
		header = convertToDataByte([1,1,1,1],["ff","ff","ff","2"])
		footer = convertToDataByte([1,1],["ff","3"])
		length = convertToDataByte([2],[hex(length)])
		tis_type = convertToDataByte([1],[hex(tis_type)])
		tisData = header + length + tis_type +tis_raw_data+footer
		return tisData	

def TIS_Heure():
	print ("Heure")
	DataTemplate = [1,1,1,1,1,1]
	if(len(sys.argv) < (len(DataTemplate) + offset)):
		noArgsMessage_tis_HeureData()
	else:
		rawData = convertToDataByte(DataTemplate,sys.argv[offset:])
		TIS_FormatData(10,sum(DataTemplate)+9,rawData)		

def TIS_Localisation():
	print ("Localisation")
	DataTemplate = [1,1,1,1,1,1,4,2,1,1,1,1,3,1,1,1]
	if(len(sys.argv) < (len(DataTemplate) + offset)):
		noArgsMessage_tis_LocalisationData()
	else:
		rawData = convertToDataByte(DataTemplate,sys.argv[offset:])
		print "rawData"
		print rawData
		TIS_FormatData(11,sum(DataTemplate)+9,rawData)		

def TIS_Course():
	print ("Course")
	DataTemplate = [1,1,1,1,1,1,4,2,16,16,1,1,1]
	if(len(sys.argv) < (len(DataTemplate) + offset)):
		noArgsMessage_tis_CourseData()
	else:
		rawData = convertToDataByte(DataTemplate,sys.argv[offset:])
		TIS_FormatData(12,sum(DataTemplate)+9,rawData)

def TIS_Arret():
	print ("Arret")
	DataTemplate = [1,1,1,1,1,1,1,3,3,3,3,3]
	if(len(sys.argv) < (len(DataTemplate) + offset)):
		noArgsMessage_tis_ArretData()
	else:
		rawData = convertToDataByte(DataTemplate,sys.argv[offset:])
		TIS_FormatData(13,sum(DataTemplate)+9,rawData)

def TIS_information_voyageurs():
	print ("not implemented yet")

def TIS_etat_detresse():
	print ("not implemented yet")

def TIS_localisation_gps():
	print ("not implemented yet")

def TIS_etat_capteur_localisation():
	print ("not implemented yet")

def TIS_information_deviation():
	print ("not implemented yet")

def TIS_SAE():
	print ("TIS_SAE")
	if(len(sys.argv) < 3):
		noArgsMessage_tis_FrameType()
	else:
		FRAMETYPE = int(sys.argv[2],16)
		if(FRAMETYPE == 10):
			TIS_Heure()
		elif(FRAMETYPE == 11):
			TIS_Localisation()
		elif(FRAMETYPE == 12):
			TIS_Course()
		elif(FRAMETYPE == 13):
			TIS_Arret()
		elif(FRAMETYPE == 14):
			TIS_information_voyageurs()
		elif(FRAMETYPE == 15):
			TIS_etat_detresse()
		elif(FRAMETYPE == 16):
			TIS_localisation_gps()
		elif(FRAMETYPE == 17):
			TIS_etat_capteur_localisation()
		elif(FRAMETYPE == 18):
			TIS_information_deviation()	

class TisFrame:
	m_type = 11
	m_size = 31
	m_HeureDataFrameTemplate = collections.OrderedDict()
	m_LocalisationDataFrameTemplate = collections.OrderedDict()
	m_CourseDataFrameTemplate = collections.OrderedDict()
	m_ArretDataFrameTemplate = collections.OrderedDict()
	m_InfVoyageurDataFrameTemplate = collections.OrderedDict()
	m_EtatDetresDataFrameTemplate = collections.OrderedDict()
	m_LocalisationGPSDataFrameTemplate = collections.OrderedDict()
	m_EtatCaptLocalDataFrameTemplate = collections.OrderedDict()
	m_DataFrameTemplate = collections.OrderedDict()
	
	m_HeureDataFrameTemplate = [
					("ANNEE",[7,1,118,118,0,255]),
					("MOIS",[8,1,1,1,1,12]),
					("JOUR",[9,1,1,1,1,31]),
					("HEURE",[10,1,1,1,0,23]),
					("MINUTE",[11,1,1,1,0,59]),
					("SECONDES",[12,1,1,1,0,59]),
					]
	m_LocalisationDataFrameTemplate = [
					("ANNEE",[7,1,118,118,0,255]),
					("MOIS",[8,1,1,1,1,12]),
					("JOUR",[9,1,1,1,1,31]),
					("HEURE",[10,1,1,1,0,23]),
					("MINUTE",[11,1,1,1,0,59]),
					("SECONDES",[12,1,1,1,0,59]),
					("NUM_COND",[13,4,0,0,0,99999]),
					("NUM_PARC",[17,2,0,0,0,65535]),
					("ETAT_EXPL",[19,1,0,0,0,3]),
					("ETAT_LOC",[20,1,0,0,0,10]),
					("TPS_ATT_TERM",[21,1,0,0,0,99]),
					("NUM_CRSE",[22,1,0,0,0,99]),
					("NUM_ARRET",[23,3,0,0,0,99999]),
					("ZONE",[26,1,0,0,0,255]),
					("DEPOT",[27,1,0,0,0,1]),
					("TYPE_VEHIC",[28,1,0,0,1,4]),
					]
	m_CourseDataFrameTemplate = [
					("ANNEE",[7,1,118,118,0,255]),
					("MOIS",[8,1,1,1,1,12]),
					("JOUR",[9,1,1,1,1,31]),
					("HEURE",[10,1,1,1,0,23]),
					("MINUTE",[11,1,1,1,0,59]),
					("SECONDES",[12,1,1,1,0,59]),
					("NUM_LIGNE",[13,4,48,48,48,48]),
					("NUM_CHAIN",[17,2,0,0,0,9999]),
					("DESTINATION",[19,16,48,48,48,48]),
					("VIA",[35,16,48,48,48,48]),
					("LOGE_ACTIVE",[51,1,0,0,0,2]),
					("NUM_CRSE",[52,1,0,0,0,99]),
					("SENS",[53,1,0,0,0,2]),
					]
	m_ArretDataFrameTemplate = [
					("ANNEE",[7,1,118,118,0,255]),
					("MOIS",[8,1,1,1,1,12]),
					("JOUR",[9,1,1,1,1,31]),
					("HEURE",[10,1,1,1,0,23]),
					("MINUTE",[11,1,1,1,0,59]),
					("SECONDES",[12,1,1,1,0,59]),
					("POSITION",[13,1,0,0,0,3]),
					("NBRE_ARRET",[14,1,0,0,0,5]),
					("NBRE_ARRET1",[15,1,0,0,0,99999]),
					("NBRE_ARRET2",[18,1,0,0,0,99999]),
					("NBRE_ARRET3",[21,1,0,0,0,99999]),
					("NBRE_ARRET4",[24,1,0,0,0,99999]),
					("NBRE_ARRET5",[27,1,0,0,0,99999]),

					]

	def __init__(self,frame_type):
		if 10 <= frame_type <= 18 :
			self.m_type = frame_type
			if (self.m_type == 10):
				self.m_DataFrameTemplate = self.m_HeureDataFrameTemplate
			elif (self.m_type == 11):
				print ("Localisation frame")
				self.m_DataFrameTemplate = self.m_LocalisationDataFrameTemplate
			elif (self.m_type == 12):
				self.m_DataFrameTemplate = self.m_CourseDataFrameTemplate
			else :
				print ("tisseo localisation frame : frame type is not supported yet")
		else :
			print ("tisseo localisation frame : frame type is not supported")
			quit()
	
	def set_field(self,fieldName,fieldValue):
		l_DataFrameTemplate = dict(self.m_DataFrameTemplate)
		if fieldName in l_DataFrameTemplate:
			if type(fieldValue) is type(l_DataFrameTemplate[fieldName][3]):
				if ((type(fieldValue) is int) and (l_DataFrameTemplate[fieldName][4] <= fieldValue <= l_DataFrameTemplate[fieldName][5])):
					print "tisseo frame : setting {0} with {1}".format(fieldName,fieldValue)
					l_DataFrameTemplate[fieldName][3] = fieldValue
					return 1
				else :
					print "tisseo frame : value out of range"
					return -1
			else:
				print "tisseo frame : Not same type"
				return -1
		else:
			print "tisseo frame : Item not found"
			return -1
	
	def get_field(self,fieldName):
		l_DataFrameTemplate = dict(self.m_DataFrameTemplate)
		return l_DataFrameTemplate[fieldName][3]
	
	def send(self,tcp_ip,tcp_port):
		l_DataFrameTemplate = dict(self.m_DataFrameTemplate)
		self.m_size = sum([x[1] for x in l_DataFrameTemplate.values()]) + 9
		rawData = convertToDataByte2(self.m_DataFrameTemplate)
		tis_data = TIS_FormatData(self.m_type,self.m_size,rawData)
		try:
			sendTCPData(tis_data,tcp_ip,tcp_port)
		except:
			print "Can't connect to host"

	def tis_print(self):
		rawData = convertToDataByte2(self.m_DataFrameTemplate)
		print TIS_FormatData(self.m_type,self.m_size,rawData)
		
		
	
		

