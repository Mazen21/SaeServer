#!/usr/bin/env python
import socket
import sys
import binascii
from saeserver_generic import *

def noArgsMessage_udp_FrameType():
	print ("|offset|Taille|Champ      |intervalle|Default|")
	print ("|7     |1     |ANNE       |0<->255   |100    |")
	print ("|8     |1     |MOIS       |1<->12    |1      |")
	print ("|9     |1     |JOUR       |1<->31    |1      |")
	print ("|10    |1     |HEURE      |0<->23    |1      |")
	print ("|11    |1     |MINUTE     |0<->59    |1      |")
	print ("|12    |1     |SECONDES   |0<->59    |1      |")	
	print ("|13    |4     |NUM_COND   |0<->999999|0      |")	
	print ("|17    |2     |NUM_PARC   |0<->99999 |1      |")	
	print ("|19    |16    |TYPE_EXPL1 |0<->7     |0      |")	
	print ("|19    |16    |TYPE_EXPL2 |0<->4     |0      |")	
	print ("|51    |1     |NUM_LIGNE  |0<->250   |0      |")	
	print ("|52    |1     |NUM_CHAIN  |0<->999   |0      |")	
	print ("|53    |1     |SENS       |0<->2     |0      |")
	print ("|53    |1     |NUM_VOIT   |0<->99    |0      |")
	print ("|53    |1     |NUM_ARRET  |TEXT 8char|0      |")
	print ("|53    |1     |DEPOT      |0<->1     |0      |")
	print ("|53    |1     |MNE        |text 6    |0      |")
	print ("|53    |1     |HOR        |chiffre   |0      |")

def UDP_FormatData(udp_raw_data):
		UDPData = [0]
		header = convertToDataByte([1,1],["28","01"])
		UDPData = header + udp_raw_data
		return UDPData

class UDPFrame:
	m_size = 36
	m_FrameTemplate = collections.OrderedDict()
	m_DataFrameTemplate = collections.OrderedDict()	
	#check with text fields
	m_FrameTemplate = [
					("ANNEE",[2,1,118,118,0,255]),
					("MOIS",[3,1,1,1,1,12]),
					("JOUR",[4,1,1,1,1,31]),
					("HEURE",[5,1,1,1,0,23]),
					("MINUTE",[6,1,1,1,0,59]),
					("SECONDES",[7,1,1,1,0,59]),
					("NUM_COND",[8,3,0,0,0,999999]),
					("NUM_PARC",[11,3,0,0,0,99999]),
					("TYPE_EXPL1",[14,1,0,0,0,7]),
					("TYPE_EXPL2",[15,1,0,0,0,4]),
					("NUM_LIGNE",[16,1,0,0,0,250]),
					("NUM_CHAIN",[17,2,0,0,0,999]),
					("SENS",[19,1,0,0,0,2]),
					("NUM_VOIT",[20,1,0,0,0,99]),
					("NUM_ARRET",[21,8,0,0,0,2]),
					("DEPOT",[29,1,0,0,0,1]),
					("MNE",[30,6,0,0,0,6]),
					("HOR",[36,4,0,0,0,4]),
					]
	def __init__(self):
		self.m_DataFrameTemplate = self.m_FrameTemplate
	
	def set_field(self,fieldName,fieldValue):
		l_DataFrameTemplate = dict(self.m_DataFrameTemplate)
		if fieldName in l_DataFrameTemplate:
			if type(fieldValue) is type(l_DataFrameTemplate[fieldName][3]):
				if ((type(fieldValue) is int) and (l_DataFrameTemplate[fieldName][4] <= fieldValue <= l_DataFrameTemplate[fieldName][5])):
					print "ineo frame : setting {0} with {1}".format(fieldName,fieldValue)
					print fieldValue
					l_DataFrameTemplate[fieldName][3] = fieldValue
					return 1
				else :
					print "ineo frame : setting {0} with {1}".format(fieldName,fieldValue)
					converted =  convertToDataByte([1,1,1,1,1,1,1,1],fieldValue)
					l_DataFrameTemplate[fieldName][3] = converted
					print "Field Value {0}".format(l_DataFrameTemplate[fieldName][3])
					return -1
			else:
				#~ print TextArrayToBytes(fieldValue,8)
				return -1
		else:
			print "ineo frame : Item not found"
			return -1

	def get_field(self,fieldName):
		l_DataFrameTemplate = dict(self.m_DataFrameTemplate)
		return l_DataFrameTemplate[fieldName][3]

	def udp_print(self):
		rawData = convertToDataByte2(self.m_DataFrameTemplate)
		print UDP_FormatData(rawData)
		
	def send_udp(self,udp_ip,udp_port):
		l_DataFrameTemplate = dict(self.m_DataFrameTemplate)
		self.m_size = sum([x[1] for x in l_DataFrameTemplate.values()]) + 2
		rawData = convertToDataByte2(self.m_DataFrameTemplate)
		udp_data = UDP_FormatData(rawData)
		try:
			sendUDPData(udp_data,udp_ip,udp_port)
		except:
			print "Can't connect to host"
		
		
		
		
		
		
		
		
		
		
