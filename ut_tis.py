#!/usr/bin/env python

import socket
import sys
import time
import os
from saeserver_generic import *
from tis_sae import *


def ut_tis_1():
	f = TisFrame(11)
	f.set_field("NUM_COND",4)
	f.set_field("ETAT_EXPL",1)
	f.set_field("ETAT_LOC",2)
	f.tis_print()
	#~ f.send(TCP_IP,TCP_PORT)
	#~ checkExpectedTRA("17 0B 03 00 00 00 00 00 00 04 00 00 00 00 00 FF 00 00 00 00 00 00 00",os.path.basename(__file__),sys._getframe().f_code.co_name,"step 1")
	
	
def ut_tis_2():
	f = TisFrame(11)
	f.set_field("NUM_COND",3)
	f.set_field("ETAT_EXPL",1)
	f.set_field("ETAT_LOC",2)
	f.send('192.168.0.6',TCP_PORT)
	checkExpectedTRA("17 0B 03 00 00 00 00 00 00 03 00 00 00 00 00 FF 00 00 00 00 00 00 00",os.path.basename(__file__),sys._getframe().f_code.co_name,"step 1")
	f.set_field("NUM_COND",4)
	f.send(TCP_IP,TCP_PORT)
	checkExpectedTRA("17 0B 03 00 00 00 00 00 00 04 00 00 00 00 00 FF 00 00 00 00 00 00 00",os.path.basename(__file__),sys._getframe().f_code.co_name,"step 2")

def ut_tis_3():
	f = TisFrame(11)
	f.set_field("NUM_COND",3)
	f.set_field("ETAT_EXPL",1)
	f.set_field("ETAT_LOC",2)
	f.send('192.168.0.6',TCP_PORT)
	checkExpectedTRA("17 0B 03 00 00 00 00 00 00 03 00 00 00 00 00 FF 00 00 00 00 00 00 00",os.path.basename(__file__),sys._getframe().f_code.co_name,"step 1")
	f.set_field("NUM_COND",0)
	f.set_field("ETAT_LOC",0)
	f.send(TCP_IP,TCP_PORT)
	checkExpectedTRA("17 0B 00 00 00 00 00 00 00 00 00 00 00 00 00 FF 00 00 00 00 00 00 00",os.path.basename(__file__),sys._getframe().f_code.co_name,"step 2")

#Add test call function here
ut_tis_1()
#~ ut_tis_2()
#~ ut_tis_3()

