#!/usr/bin/env python

import socket
import sys
import time
import os
from saeserver_generic import *
from udp_sae import *

def ut_udp_1():
	f = UDPFrame()
	f.set_field("NUM_COND",4)
	f.set_field("NUM_PARC",1)
	f.set_field("SENS",2)
	f.udp_print()
	f.send_udp(UDP_IP,UDP_PORT)
	
ut_udp_1()
